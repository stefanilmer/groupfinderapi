const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GearSchema = new Schema({
  name: {
    type: String,
    required: [true, "Name is required"]
  },
  levelRequirement: {
    type: Number,
    required: [true, "levelrequirement is required"]
  },
  itemLevel: {
    type: Number,
    required: [true, "an item level is required"]
  },
  armour: {
    type: Number
  },
  primaryAttribute: {
    type: Number
  },
  secondaryAttribute: {
    type: Number
  }
});

module.exports = GearSchema;
