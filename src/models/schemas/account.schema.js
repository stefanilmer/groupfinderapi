const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

const AccountSchema = new Schema({
  username: {
    type: String,
    required: [true, "Name is required"],
    unique: [true, "This username is already in use"]
  },

  email: {
    type: String,
    required: [true, "Name is required"],
    unique: [true, "This email is already in use"]
  },

  password: {
    type: String,
    required: [true, "Password is required"]
  },

  friendsList: {
    type: [Schema.Types.ObjectId],
    ref: "account",
    default: []
  },

  characterList: {
    type: [Schema.Types.ObjectId],
    ref: "character",
    default: []
  },

  favorite: {
    type: Schema.Types.ObjectId,
    ref: "character"
  }
});

function autoPopulate(next) {
  this.populate({
    path: "friendsList",
    model: "account",
    select: "_id, username"
  });

  this.populate({
    path: "characterList",
    model: "character"
  });

  next();
}

AccountSchema.pre("findOne", autoPopulate).pre("find", autoPopulate);
AccountSchema.plugin(uniqueValidator);

module.exports = AccountSchema;
