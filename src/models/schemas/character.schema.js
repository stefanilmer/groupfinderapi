const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CharacterSchema = new Schema({
  name: {
    type: String,
    required: [true, "Name is required"]
  },

  level: {
    type: Number,
    required: [true, "level is required"]
  },

  avarageItemLevel: {
    type: Number,
    required: [true, "Avarage item level is required"]
  },

  equipment: {
    type: [gearSchema]
  },

  pveProgression: {
    type: [pveContentSchema]
  },

  pvpProgression: {
    type: [pvpContentSchema]
  }
});

module.exports = CharacterSchema;
