const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GroupSchema = new Schema({
  title: {
    type: String,
    required: [true, "title is required"]
  },
  faction: {
    type: String,
    enum: ["Alliance", "Horde"],
    default: "Alliance",
    required: [true, "Please choose a faction"]
  },
  requiredLevel: {
    type: Number,
    required: [true, "please choose a minimal required level"]
  },
  requiredItemLevel: {
    type: Number,
    required: [true, "please choose a minimal required item level"]
  },
  groupSize: {
    type: Number
  },
  partyMembers: {
    type: Number
  },
  typeOfContent: {
    type: String,
    required: [true, "Please choose a content type."]
  }
});

module.exports = GroupSchema;
