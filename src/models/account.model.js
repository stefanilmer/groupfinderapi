const mongoose = require("mongoose");
const AccountSchema = require("./schemas/account.schema");

const Account = mongoose.model("account", AccountSchema);

module.exports = Account;
